"""
Usage:
  concepts.py <category_name> <code_fname>...

  The program expects a name (for organization) and then one or more
  files containing code to be analyzed. The script generates pickled
  ConceptVisitor classes containing the analyses.
"""

import sys
import os

import pickle
import ast
from collections import OrderedDict


class ConceptVisitor(ast.NodeVisitor):
    '''Adds all 'concepts' from the code to a dictionary.
    '''

    BASE_TYPES = [int, float, bool, tuple, list, range, str, dict, open('concepts.py', 'r')]
    TYP_DICT = OrderedDict([('int', '~~Integers'), ('float', '~~Floats'), ('bool', '~~Booleans'),
                            ('tuple', '~~Tuples'), ('list', '~~Lists'), ('range', '~~Ranges'),
                            ('str', '~~Strings'), ('dict', '~~Dictionaries'),
                            ('open', '<Files>'), ('set', '~~Sets')])     # set moved to the end to remove it from base_types

    def __init__(self, src):
        self.src = src

        # Measures of length
        self.loc = 0

        # Elements identified from the AST
        self.syntax_elements = set()
        self.builtins = set()

        # Tags and extra names (not listed as a 'concept component')
        self.tags = set()
        self.modules = set()
        self.members = set()
        self.manual_check = set()

        # Namespaces and depth
        self.nest = []
        self.namespaces = [set()]
        self.named_functions = []
        self.named_classes = []
        self.has_return = False

        self.visit(ast.parse(self.src))

    def all_concepts(self):
        return set().union(self.syntax_elements, self.builtins, self.tags, self.modules, self.members, self.manual_check)

    def all_concept_components(self):
        return set().union(self.syntax_elements, self.builtins)

    def _assign_type(self, node):
        if type(node) in [ast.Num, ast.Str, ast.NameConstant]:
            expr_type = 'Literal'
        elif type(node) in [ast.List, ast.Tuple, ast.Dict]:
            names = self._get_varnames(node)
            if len(names) == 0:
                expr_type = 'Literal'
            else:
                expr_type = 'Expression'
        elif type(node) in [ast.Name, ast.Attribute]:
            expr_type = 'Variable'
        elif type(node) is ast.Call:
            expr_type = 'FuncCall'
        elif type(node) in [ast.BinOp, ast.UnaryOp, ast.Subscript, ast.BoolOp, ast.ListComp, ast.Compare]:
            expr_type = 'Expression'
        else:
            raise NotImplementedError('_assign_type: unexpected RHS expression ({0})'.format(type(node)))
        return expr_type

    def _get_varnames(self, node):
        if type(node) in [ast.Num, ast.Str, ast.NameConstant]:
            return []
        elif isinstance(node, ast.Name):
            return [node.id]
        elif type(node) in [ast.Tuple, ast.List]:
            name_elts = node.elts[:]
        elif isinstance(node, ast.ListComp):
            name_elts = [node.elt] + [gen.iter for gen in node.generators]
        elif isinstance(node, ast.Dict):
            name_elts = node.keys + node.values
        elif isinstance(node, ast.Compare):
            name_elts = [node.left] + node.comparators[:]
        elif isinstance(node, ast.BinOp):
            name_elts = [node.left, node.right]
        elif isinstance(node, ast.BoolOp):
            name_elts = node.values[:]
        elif isinstance(node, ast.UnaryOp):
            name_elts = [node.operand]
        elif isinstance(node, ast.arguments):
            if node.kwarg or node.kwonlyargs or node.kw_defaults:
                raise NotImplementedError('_get_varnames: unexpected argument type')
            args = [n.arg for n in node.args]
            if node.vararg:
                args.append(node.vararg.arg)
            return args
        elif isinstance(node, ast.Call):
            if node.starargs or node.kwargs:
                raise NotImplementedError('_get_varnames: unexpected argument type in a call')
            name_elts = node.args[:] + [kw.value for kw in node.keywords]
        elif isinstance(node, ast.Subscript):
            self.manual_check.add('Assignment: Subscript')
            return []
        elif isinstance(node, ast.Attribute):
            self.manual_check.add('Assignment: Attribute')
            return []
        else:
            raise NotImplementedError('_get_varnames: unexpected target type ({0})'.format(type(node)))

        names = []
        for n in name_elts:
            names.extend(self._get_varnames(n))
        return names

    def _check_call(self, node, suffix=''):
        # NOTE: adding names to builtins, modules, and methods
        # Checking calls of functions to see if they are user defined
        if isinstance(node, ast.Name) and hasattr(__builtins__, node.id):
            self.builtins.add('{0}{1}'.format(node.id, suffix))

            if node.id in list(ConceptVisitor.TYP_DICT.keys()):
                self.tags.add(ConceptVisitor.TYP_DICT[node.id])
                if node.id in ['int', 'float']:
                    self.syntax_elements.add('Numbers')
                elif node.id in ['list', 'str', 'tuple', 'range']:
                    self.syntax_elements.add('Sequences')
                elif node.id in ['dict', 'set']:
                    self.syntax_elements.add('Mappings')

            return ''
        elif isinstance(node, ast.Attribute):
            base_name = ConceptVisitor._root_name(node)
            name = node.attr

            if ConceptVisitor._is_module(base_name):
                self.modules.add(base_name)
                self.members.add('{0}{1} ({2})'.format(name, suffix, base_name))
                return ''
            else:
                if not name.startswith('__'):          # eliminate things like __init__
                    for (index, typ) in enumerate(ConceptVisitor.BASE_TYPES):
                        if hasattr(typ, name):
                            self.members.add('{0}{1}'.format(name, suffix))
                            self.tags.add(list(ConceptVisitor.TYP_DICT.values())[index])
                            if list(ConceptVisitor.TYP_DICT.keys())[index] in ['int', 'float']:
                                self.syntax_elements.add('Numbers')
                            elif list(ConceptVisitor.TYP_DICT.keys())[index] in ['list', 'str', 'tuple', 'range']:
                                self.syntax_elements.add('Sequences')
                            elif list(ConceptVisitor.TYP_DICT.keys())[index] in ['dict', 'set']:
                                self.syntax_elements.add('Mappings')
                            return False
                return name
        elif isinstance(node, ast.Name):
            return node.id
        else:
            raise NotImplementedError('unexpected node type in callee name location')

    def _check_nest(self, current_typ):
        for typ in ['If', 'Else', 'While', 'For']:
            if typ in self.nest:
                self.tags.add('<{0}->{1}>'.format(typ, current_typ))

    def _is_module(module):
        # Only works for modules that are installed on the system
        try:
            __import__(module)
        except ImportError:
            return False
        else:
            return True

    def _root_name(node):
        if isinstance(node, ast.Name):
            return node.id
        elif isinstance(node, ast.Attribute):
            return ConceptVisitor._root_name(node.value)
        elif isinstance(node, ast.Call):
            return ConceptVisitor._root_name(node.func)
        elif isinstance(node, ast.Str):
            return 'str'
        elif isinstance(node, ast.Subscript):
            return ConceptVisitor._root_name(node.value)
        else:
            raise NotImplementedError('_root_name: Unanticipated node encountered ({0})'.format(type(node)))

    def generic_visit(self, node):
        print('{0} visitor not defined'.format(type(node).__name__), file=sys.stderr)
        print(node._fields)

    def visit_Add(self, node):
        self.syntax_elements.add('+')
        self.tags.add('<Arithmetic Op>')
        super().generic_visit(node)

    def visit_alias(self, node):
        # name -> name being imported
        # asname -> name to import as or None
        # NOTE: used by imports and ... ? (Not 'with' -- that uses a 'withclause')
        # NOTE: not importing module names here because it is used by Import and FromImport
        if node.asname:
            self.syntax_elements.add('as (import)')
        super().generic_visit(node)

    def visit_arg(self, node):
        # arg -> str name
        # annotation -> None???
        if node.annotation:
            raise NotImplementedError('visit_arg: annotation encountered')
        super().generic_visit(node)

    def visit_arguments(self, node):
        # args -> list of arguments
        # vararg -> None or list of args
        # kwonlyargs -> list of ???
        # kw_defaults -> list of ???
        # kwarg -> None???
        # defaults -> list of values for default parameters
        if node.kwonlyargs or node.kw_defaults or node.kwarg:
            raise NotImplementedError('visit_arguments: unimplemented argument options')
        if node.defaults:
            self.tags.add('<~~with defaults>')
        if node.vararg:
            self.tags.add('<~~with varargs>')
        if node.args or node.vararg:
            self.tags.add('<~~with params>')
        else:
            self.tags.add('<~~no params>')
        super().generic_visit(node)

    def visit_And(self, node):
        self.syntax_elements.add('and')
        super().generic_visit(node)

    def visit_Assign(self, node):
        # node.targets -> list of nodes, LHS targets
        # node.value -> node, RHS
        if len(node.targets) != 1:
            raise NotImplementedError('visit_Assign: unexpected number of targets')

        self.tags.add('Assignment')

        if isinstance(node.targets[0], ast.Tuple):
            self.tags.add('~~Tuples')
            expr_type = 'Tuple'
        else:
            expr_type = self._assign_type(node.value)
        self.tags.add('~~of {0}'.format(expr_type))

        target_names = self._get_varnames(node.targets[0])
        source_names = self._get_varnames(node.value)
        if set(target_names).intersection(source_names):
            target_type = 'Update'
        elif set(target_names).intersection(self.namespaces[-1]):
            target_type = 'Reassignment'
        else:
            target_type = 'Initial'
        self.tags.add('~~{0}'.format(target_type))
        self.namespaces[-1].update(target_names)

        self.syntax_elements.add('=')
        super().generic_visit(node)

    def visit_Attribute(self, node):
        # value -> Name of parent module
        # attr -> str name of module member being used
        self.syntax_elements.add('.')
        if isinstance(node.value, ast.Name) and node.value.id is 'self':
            self.tags.add('<~~using self>')
        super().generic_visit(node)

    def visit_AugAssign(self, node):
        # target -> LHS (often a Name)
        # op -> operator, the augmented operation
        # value -> RHS
        expr_type = self._assign_type(node.value)
        self.tags.add('Assignment')
        self.tags.add('~~Update')
        self.tags.add('~~of {0}'.format(expr_type))
        self.namespaces[-1].update(self._get_varnames(node.target))

        if type(node.op) is ast.Add:
            self.syntax_elements.add('+=')
        elif type(node.op) is ast.Sub:
            self.syntax_elements.add('-=')
        elif type(node.op) is ast.Mult:
            self.syntax_elements.add('*=')
        elif type(node.op) is ast.Div:
            self.syntax_elements.add('/=')
        else:
            raise NotImplementedError('visit_AugAssign: unexpected operator ({0})'.format(type(node.op)))

        super().generic_visit(node)

    def visit_BinOp(self, node):
        # left -> LHS
        # op -> operator
        # right -> RHS
        if type(node.left) in [ast.BinOp, ast.UnaryOp, ast.BoolOp] or \
           type(node.right) in [ast.BinOp, ast.UnaryOp, ast.BoolOp]:
            self.manual_check.add('Order of Operations')
        super().generic_visit(node)

    def visit_BoolOp(self, node):
        # op -> operator
        # values -> list of LHS, RHS?
        self.tags.add('~~Booleans')
        self.tags.add('<Boolean Op>')

        for val in node.values:
            if type(val) in [ast.BinOp, ast.UnaryOp, ast.BoolOp, ast.Compare]:
                self.manual_check.add('Order of Operations')
        super().generic_visit(node)

    def visit_Break(self, node):
        self.syntax_elements.add('break')
        super().generic_visit(node)

    def visit_Call(self, node):
        # func -> Name
        # args -> list of nodes, the arguments
        # keywords -> list of (nodes?)
        # starargs -> None (node?)
        # kwargs -> None (node?)
        user_defined_name = self._check_call(node.func, '()')

        self.syntax_elements.add('Function Call')
        if user_defined_name:
            if user_defined_name in self.named_classes:
                self.syntax_elements.add('Instances')
            self.tags.add('<Defined Call>')
        if node.args or node.keywords:
            if node.args:
                self.tags.add('<Function Call (with arguments)>')
                if user_defined_name:
                    self.tags.add('<~~with arguments>')
            if node.keywords:
                self.tags.add('<Function Call (with keyword arguments)>')
                if user_defined_name:
                    self.tags.add('<~~with keywords>')
        else:
            self.tags.add('<Function Call (no arguments)>')
            if user_defined_name:
                self.tags.add('<~~no arguments>')

        if isinstance(node.func, ast.Name):
            name = node.func.id
        elif isinstance(node.func, ast.Attribute):
            name = node.func.attr
        else:
            raise NotImplementedError('visit_Call: unexpected function name')

        if node.starargs or node.kwargs:
            raise NotImplementedError('visit_Call: unexpected use of arguments')

        if 'Call' in self.nest:
            self.tags.add('<Call(Call())>')
        if 'Func:{0}'.format(name) in self.nest and name != '__init__':
            self.tags.add('<Recursive Call>')

        self.nest.append('Call')
        self.visit(node.func)
        for n in node.args:
            self.visit(n)
        for n in node.keywords:
            self.visit(n)
        # NOTE: no starargs or kwargs
        self.nest.pop()

    def visit_ClassDef(self, node):
        # name -> str name of class
        # bases -> list of parent classes
        # keywords -> list of ???
        # starargs -> None???
        # kwargs -> None???
        # body -> list of members
        # decorator_list -> list of ???
        if node.keywords or node.starargs or node.kwargs or node.decorator_list:
            raise NotImplementedError("visit_ClassDef: unimplemented class feature detected")
        self.named_functions.append(node.name)    # For constructor
        self.named_classes.append(node.name)      # To identify constructors as compared to functions

        self.loc += len(node.body)
        self.syntax_elements.add('Classes')
        self.syntax_elements.add('class')

        if node.bases:
            if len(node.bases) == 1:
                if node.bases[0].id.lower() != 'object':
                    self.tags.add('<~~with inheritance>')
                else:
                    self.tags.add('<~~w/o inheritance>')
            else:
                self.tags.add('<~~with multiple inheritance>')
        else:
            self.tags.add('<~~w/o inheritance>')

        self.namespaces.append(set())
        self.nest.append('ClassDef')
        if self.nest.count('ClassDef') >= 2:
            self.tags.add('<Class->Class>')
        super().generic_visit(node)
        self.nest.pop()
        self.namespaces.pop()

    def visit_Compare(self, node):
        # left -> Call, because LHS needs to call the operator ...
        # ops -> list of operator(s) being invoked
        #        Note: in compound operators like "2 < x < 5", multiple operators are required
        # comparators -> values being compared against (RHS)
        self.tags.add('<Comparison>')
        if len(node.ops) > 1:
            self.tags.add('<Compound Comparison>')
        super().generic_visit(node)

    def visit_comprehension(self, node):
        # target -> Name of variable defined in comp
        # iter -> value being iterated over
        # ifs -> List of filters
        if node.ifs:
            raise NotImplementedError("visit_comprehension: unexpected use of if clause in comprehension")
        super().generic_visit(node)

    def visit_Dict(self, node):
        # keys --> list of keys
        # values --> list of values
        self.tags.add('~~Dictionaries')
        self.syntax_elements.add('Mappings')
        super().generic_visit(node)

    def visit_Div(self, node):
        self.syntax_elements.add('/')
        self.tags.add('<Arithmetic Op>')
        super().generic_visit(node)

    def visit_Eq(self, node):
        self.syntax_elements.add('==')
        super().generic_visit(node)

    def visit_ExceptHandler(self, node):
        # type -> Name, the type of exception, or None, if a generic case
        # name -> None or the str name of the exception
        # body -> list of statements in the handler
        self.loc += len(node.body)
        self.syntax_elements.add('try-except')
        self.syntax_elements.add('Exceptions')

        if node.type:    # Named exception
            self._check_call(node.type)
            self.tags.add('<~~named handler>')
        else:
            self.tags.add('<~~general handler>')

        if node.name:
            self.syntax_elements.add('as (exception)')
        super().generic_visit(node)

    def visit_Expr(self, node):
        # value -> node, Why a single node?
        super().generic_visit(node)

    def visit_For(self, node):
        # target -> loop variable name
        # iter -> iterable object
        # body -> list of statements
        # orelse -> None or list of nodes
        self.loc += len(node.body) + len(node.orelse)
        self.syntax_elements.add('for')
        self.tags.add('<Loops>')
        if node.orelse:
            self.syntax_elements.add('else (for)')

        self.namespaces[-1].update(self._get_varnames(node.target))
        self._check_nest('For')
        self.nest.append('For')
        super().generic_visit(node)
        self.nest.pop()

    def visit_FloorDiv(self, node):
        self.syntax_elements.add('//')
        self.tags.add('<Arithmetic Op>')
        super().generic_visit(node)

    def visit_FunctionDef(self, node):
        # name -> str name
        # args -> arguments object
        # body -> list of statements
        # decorator_list -> list of ???
        # returns -> None / expression object that represents the return ANNOTATION
        if node.decorator_list:
            raise NotImplementedError('visit_FunctionDef: unexpected function definition')
        self.named_functions.append(node.name)

        self.loc += len(node.body)
        self.syntax_elements.add('def')

        if node.name.startswith('__'):
            self.members.add('{0}()'.format(node.name))

        if node.returns:
            self.tags.add('<~~with function annotation>')
            raise NotImplementedError('Detected a function annotation')

        if 'FunctionDef' in self.nest:
            self.tags.add('<Func->Func>')

        self.namespaces.append(set())
        self.namespaces[-1].update(self._get_varnames(node.args))

        self.nest.append('FunctionDef')
        self.nest.append('Func:{0}'.format(node.name))
        prev_has_return = self.has_return
        self.has_return = False
        super().generic_visit(node)
        if not self.has_return:
            self.tags.add('<~~implied>')
        self.has_return = prev_has_return
        self.nest.pop()    # name
        self.nest.pop()    # FunctionDef
        self.namespaces.pop()

    def visit_GtE(self, node):
        self.syntax_elements.add('>=')
        super().generic_visit(node)

    def visit_Gt(self, node):
        self.syntax_elements.add('>')
        super().generic_visit(node)

    def visit_If(self, node):
        # test -> Compare
        # body -> list of nodes, "then"
        # orelse -> list of nodes, "else"
        # Note: elif is indistinguishable from an else: if
        self.loc += len(node.body)
        self.syntax_elements.add('if')

        self._check_nest('If')

        self.nest.append('If')
        self.visit(node.test)
        for n in node.body:
            self.visit(n)
        self.nest.pop()

        if len(node.orelse) > 0:
            self.loc += len(node.orelse) + 1
            self.syntax_elements.add('else (if)')

            self.nest.append('Else')
            for n in node.orelse:
                self.visit(n)
            self.nest.pop()

    def visit_Import(self, node):
        # names -> list of alias objects
        # NOTE: module names have to be done here, rather than in alias, because
        #   alias is also used by ImportFrom, where the aliases are module elements
        for alias in node.names:
            self.modules.add(alias.name.split('.')[0])
        self.syntax_elements.add('import')
        super().generic_visit(node)

    def visit_ImportFrom(self, node):
        # module -> module being imported from
        # names -> list of alias objects
        # level -> ???
        if node.level != 0:
            raise NotImplementedError('ImportFrom: level field is not 0')

        self.modules.add(node.module.split('.')[0])
        self.syntax_elements.add('from')
        self.syntax_elements.add('import')
        super().generic_visit(node)

    def visit_In(self, node):
        self.syntax_elements.add('in')
        super().generic_visit(node)

    def visit_Index(self, node):
        # value -> num or None
        self.syntax_elements.add('[~]')
        super().generic_visit(node)

    def visit_Is(self, node):
        self.syntax_elements.add('is')
        super().generic_visit(node)

    def visit_keyword(self, node):
        # arg -> str keyword name
        # value -> value of argument
        super().generic_visit(node)

    def visit_Load(self, node):
        super().generic_visit(node)

    def visit_List(self, node):
        # elts -> list of values
        self.tags.add('~~Lists')
        self.syntax_elements.add('Sequences')
        super().generic_visit(node)

    def visit_ListComp(self, node):
        # elt -> Expression used to generate values in the list comp
        # generators -> List of comprehension generator (why plural?)
        if len(node.generators) != 1:
            raise NotImplementedError("visit_ListComp: unexpected number of generators")
        self.tags.add('~~Lists')
        self.syntax_elements.add('Sequences')
        self.syntax_elements.add('List Comprehension')
        super().generic_visit(node)

    def visit_LtE(self, node):
        self.syntax_elements.add('<=')
        super().generic_visit(node)

    def visit_Lt(self, node):
        self.syntax_elements.add('<')
        super().generic_visit(node)

    def visit_Mod(self, node):
        self.syntax_elements.add('% (mod)')
        self.tags.add('<Arithmetic Op>')
        super().generic_visit(node)

    def visit_Module(self, node):
        # body -> list of nodes, contents of module
        self.loc += len(node.body)
        super().generic_visit(node)

    def visit_Mult(self, node):
        self.syntax_elements.add('*')
        self.tags.add('<Arithmetic Op>')
        super().generic_visit(node)

    def visit_Name(self, node):
        # id -> str
        # ctx -> Load/Store object (memory location?)
        self.syntax_elements.add('Variable')
        super().generic_visit(node)

    def visit_NameConstant(self, node):
        # value -> value of constant
        if node.value in [True, False]:
            self.tags.add('~~Booleans')
        elif node.value is None:
            self.syntax_elements.add('None')
        else:
            raise NotImplementedError("NameConstant: unanticipated constant encountered")
        self.syntax_elements.add(str(node.value))
        super().generic_visit(node)

    def visit_Not(self, node):
        self.syntax_elements.add('~~Booleans')
        self.syntax_elements.add('not')
        super().generic_visit(node)

    def visit_NotEq(self, node):
        self.syntax_elements.add('!=')
        super().generic_visit(node)

    def visit_NotIn(self, node):
        self.syntax_elements.add('not')
        self.syntax_elements.add('~~Booleans')

        self.syntax_elements.add('in')
        super().generic_visit(node)

    def visit_Num(self, node):
        self.syntax_elements.add('Numbers')
        if isinstance(node.n, int):
            self.tags.add('~~Integers')
        elif isinstance(node.n, float):
            self.tags.add('~~Floats')
        else:
            raise NotImplementedError('Complex number encountered?')
        super().generic_visit(node)

    def visit_Or(self, node):
        self.syntax_elements.add('or')
        super().generic_visit(node)

    def visit_Pass(self, node):
        self.syntax_elements.add('pass')
        super().generic_visit(node)

    def visit_Pow(self, node):
        self.syntax_elements.add('**')
        self.tags.add('<Arithmetic Op>')
        super().generic_visit(node)

    def visit_Raise(self, node):
        # exc -> constructor being invoked
        # cause -> ???
        if node.cause:
            raise NotImplementedError('Raise: exception raised with a cause')

        # Constructed exceptions are handled by visit_Call, but raising an exception
        # class requires special handling.
        if isinstance(node.exc, ast.Name):
            self._check_call(node.exc)

        self.syntax_elements.add('raise')
        self.syntax_elements.add('Exceptions')
        super().generic_visit(node)

    def visit_Return(self, node):
        # value
        self.has_return = True
        if not node.value:
            self.tags.add('<~~void return>')
        self.syntax_elements.add('return')
        super().generic_visit(node)

    def visit_Slice(self, node):
        # lower -> Num or None
        # upper -> Num or None
        # step -> Num or None
        if node.step:
            self.syntax_elements.add('[::]')
        else:
            self.syntax_elements.add('[:]')
        super().generic_visit(node)

    def visit_Store(self, node):
        super().generic_visit(node)

    def visit_Str(self, node):
        self.tags.add('~~Strings')
        self.syntax_elements.add('Sequences')
        super().generic_visit(node)

    def visit_Subscript(self, node):
        # value -> Name object, variable being subscripted
        # slice -> Slice or Index object
        super().generic_visit(node)

    def visit_Sub(self, node):
        self.syntax_elements.add('- (subtraction)')
        self.tags.add('<Arithmetic Op>')
        super().generic_visit(node)

    def visit_Try(self, node):
        # body -> list of statements in the try clause
        # handlers -> list of exception handlers
        # orelse -> [] or the list of statements in the 'no exception' case
        # finalbody -> [] or the list of final case statements
        if node.orelse or node.finalbody:
            raise NotImplementedError('Try: contains else or final clause')

        # NOTE: in most case, type of try is added within the exception handlers
        if not node.handlers:
            raise NotImplementedError('Try: no exception clause')

        self.manual_check.add('Nesting in exception')
        self.loc += len(node.body) + len(node.handlers)
        super().generic_visit(node)

    def visit_Tuple(self, node):
        # elts -> list of values
        super().generic_visit(node)

    def visit_UnaryOp(self, node):
        # op -> operator
        # operand --> RHS
        if type(node.operand) in [ast.BinOp, ast.UnaryOp, ast.BoolOp]:
            self.manual_check.add('Order of Operations')
        super().generic_visit(node)

    def visit_USub(self, node):
        self.syntax_elements.add('- (negative)')
        self.tags.add('<Arithmetic Op>')
        super().generic_visit(node)

    def visit_While(self, node):
        # test -> comparison operation
        # body -> list of statements
        # orelse -> None or else clause
        if node.orelse:
            raise NotImplementedError('visit_While: while has an unexpected else clause')

        self.loc += len(node.body)
        self.syntax_elements.add('while')
        self.tags.add('<Loops>')

        self._check_nest('While')
        self.nest.append('While')
        super().generic_visit(node)
        self.nest.pop()

    def visit_With(self, node):
        # items -> list of items being aliased
        # body -> list of expressions in body
        self.loc += len(node.body)
        self.syntax_elements.add('with')
        self.manual_check.add('Nesting in "with"')
        super().generic_visit(node)

    def visit_withitem(self, node):
        # context_expr -> expression being evaluated (and potentially renamed)
        # optional_vars -> Name or None
        self.syntax_elements.add('as (with)')
        super().generic_visit(node)

    def __str__(self):
        all_elems = ('Syntax: {0}'.format(','.join(self.syntax_elements)),
                     'Builtins: {0}'.format(','.join(self.builtins)),
                     'Tags: {0}'.format(','.join(self.tags)),
                     'Modules: {0}'.format(','.join(self.modules)),
                     'Members: {0}'.format(','.join(self.members)),
                     'Flags: {0}'.format(','.join(self.manual_check)),
                    )
        return '\n'.join(all_elems)


if __name__ == '__main__':
    import docopt
    arguments = docopt.docopt(__doc__)
    category_name = arguments.get('<category_name>')
    code_files = arguments.get('<code_fname>')

    # Getting the concepts for each file
    results = OrderedDict()
    for fname in code_files:
        try:
            code = open(fname).read()
        except IOError:
            print('Could not open {0}'.format(fname), file=sys.stderr)
            sys.exit()

        fname = fname.split(os.sep)[-1]
        #print(category_name, fname)
        results[fname] = ConceptVisitor(code)

    with open('{0}.pickle'.format(category_name), 'wb') as outf:
        pickle.dump(results, outf)

    if category_name == 'test':
        for (fname, cv) in results.items():
            print(fname, str(cv))


