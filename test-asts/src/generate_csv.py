"""
Usage:
  generate_csv.py <category_fname>...

  The program expects one or more pickle files geneated by concepts.py and
  generates a master CSV file with all of the records.
"""

import sys

import pickle
from collections import OrderedDict

from concepts import ConceptVisitor


def gen_header(data):
    # Creating sets of all elements identified
    syntax_elements, builtins, modules, members, tags, manual_check = set(), set(), set(), set(), set(), set()
    for (category, results) in data.items():
        for cv in results.values():
            syntax_elements = syntax_elements.union(cv.syntax_elements)
            builtins = builtins.union(cv.builtins)
            modules = modules.union(cv.modules)
            members = members.union(cv.members)
            tags = tags.union(cv.tags)
            manual_check = manual_check.union(cv.manual_check)
    all_concepts = sorted(syntax_elements) + sorted(builtins) + sorted(modules) + sorted(members) + sorted(tags) + sorted(manual_check)
    all_concepts.insert(0, '')

    # Generating a CSV indicating which concepts were found in which files
    top_header = [''] * len(all_concepts)
    top_header[0] = 'File Name (lines)'
    running_len = 1
    if len(syntax_elements):
        top_header[running_len] = 'Syntax Elements'
        running_len += len(syntax_elements)
    if len(builtins):
        top_header[running_len] = 'Builtins'
        running_len += len(builtins)
    if len(modules):
        top_header[running_len] = 'Modules'
        running_len += len(modules)
    if len(members):
        top_header[running_len] = 'Members'
        running_len += len(members)
    if len(tags):
        top_header[running_len] = 'Tags'
        running_len += len(tags)
    if len(manual_check):
        top_header[running_len] = 'To Check Manually'
    return all_concepts, '{0}\n{1}'.format(','.join(top_header), ','.join(all_concepts))


def gen_csv(outf_name, data, row_func):
    with open(outf_name, 'w') as out:
        all_concepts, header = gen_header(data)
        print(header, file=out)
        for (category, results) in data.items():
            for (fname, cv) in results.items():
                row = row_func(cv, all_concepts)
                row[0] = '{0}/{1} ({2})'.format(category, fname, cv.loc)
                print(','.join(row), file=out)


def set_membership(cv, all_concepts):
    curr_items = cv.all_concepts()
    row = [str(c in curr_items) for c in all_concepts]
    return row


if __name__ == '__main__':
    import docopt
    arguments = docopt.docopt(__doc__)
    category_files = arguments.get('<category_fname>')

    data = OrderedDict()     # category: {question: ConceptVisitor}
    for cat_fname in category_files:
        data[cat_fname.split('.')[0]] = pickle.load(open(cat_fname, 'rb'))

    gen_csv('master_results.csv', data, set_membership)

    gen_csv('crosstab.csv', data, crosstab)


